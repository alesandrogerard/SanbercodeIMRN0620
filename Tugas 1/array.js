//soal nomor 1
function range(startNum, finishNum) {
    var a = []
    x = startNum
    y = finishNum
  if (x < y){
      for (startNum;startNum <= finishNum; startNum++ )
      a.push(startNum)
      return a
  }else if (x > y){
      for (startNum;startNum >= finishNum; startNum-- )
      a.push(startNum)
      return a
  }else {
      return ("-1")
    }      
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11,18)) 
console.log(range(54, 50)) 
console.log(range())

console.log("")
//soal nomor 2
function rangeWithStep(startNum, finishNum,step) {
    var a = []
    x = startNum
    y = finishNum
  if (x < y){
      for (startNum;startNum <= finishNum; startNum+= step )
      a.push(startNum)
      return a
  }else if (x > y){
      for (startNum;startNum >= finishNum; startNum-= step )
      a.push(startNum)
      return a}
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4))
console.log("")

//soal nomor 3
function sum(startNum=0, finishNum, step=1) {
    const a = []
    const reducer = (accumulator, currentValue) => 
    accumulator + currentValue
    x = startNum
    y = finishNum
    z = step
  if (x < y){
      for (startNum;startNum <= finishNum; startNum+= step )
      a.push(startNum)
      return a.reduce(reducer)
  }else if (x > y){
      for (startNum;startNum >= finishNum; startNum-= step )
      a.push(startNum)
      return a.reduce(reducer)}
   else{
      return startNum}
}

console.log(sum(1,10))
console.log(sum(5, 50, 2))
console.log(sum(15,10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("")
//soal nomor 4
//soal nomor 4
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
  ] 
function dataHandling(){
for(i = 0; i < input.length; i++){    
var id = input[i][0]
var nama = input[i][1]
var lokasi = input[i][2]
var ttl = input[i][3]
var hobi = input[i][4]
console.log("Nomor ID : "+ id+"\n"+ "Nama Lengkap : "+ nama +"\n" + "TTL : "+ ttl+"\n" + "Hobi : "+ hobi+"\n")}
}

dataHandling()
console.log("")

//soal nomor 5
function balikKata(s)  {
    var x = s.split("")
    var y = x.reverse()
    var z = y.join("")    
    return z
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers")) 
console.log("")

//soal nomor 6
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"]

function dataHandling2(x)  {
  x.splice(1,4,"Roman Alamsyah Elsharawy","Provinsi Bandar Lampung","21/05/1989","Pria","SMA Internasional Metro")
  y = x
  nama = y[1]
  tanggal = y[3].split("/")
  bulan = parseInt(tanggal[1])
  
  
  console.log(y)
  switch(bulan) {
      case 1:   { console.log(' Januari '); break; }
      case 2:   { console.log(' Februari '); break; }
      case 3:   { console.log(' Maret '); break; }
      case 4:   { console.log(' April '); break; }
      case 5:   { console.log(' Mei '); break;}
      case 6:   { console.log(' Juni '); break;}
      case 7:   { console.log(' Juli '); break;}
      case 8:   { console.log(' Agustus '); break;}
      case 9:   { console.log(' September '); break;}
      case 10:   { console.log(' Oktober '); break;}
      case 11:   { console.log(' November '); break;}
      case 12:   { console.log(' Desember '); break;}
      default:  { console.log('Masukan angka 1-12!');}
      }
  console.log(tanggal.sort((a, b) => b - a))
  tanggal.push("1989")
  console.log(tanggal.slice(1,).join("-"))
  console.log(nama.slice(0,15))
}
//join('-')

dataHandling2(input)
