//SOAL NOMOR 1
var now = new Date()
var thisYear = now.getFullYear()

function arrayToObject(arr) {
    var arrobj = {}
    if (arr.length == ""){
      console.log("")
    }else {
      for (let i = 0; i < 2; i++){
        arrobj = {
          firstName : arr[i][0],
          lastName : arr[i][1],
          gender : arr[i][2],
          age : thisYear - arr[i][3],
        }
        if (arrobj.age <= 0 || !arrobj.age ){
            arrobj.age = "Invalid Birth Year!"
        }console.log(`${i+1}. ${arrobj.firstName} : {
          firstName : ${arrobj.firstName},
          lastName : ${arrobj.lastName},
          gender : ${arrobj.gender},
          age : ${arrobj.age}}`)
        }
      }
      return arr
    } 
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

//SOAL NOMOR 2
function shoppingTime(memberId, money) {
	var barang = {'Sepatu brand Stacattu':1500000,
			'Baju brand Zoro':500000,
			'Baju brand H&N':250000,
			'Sweater brand Uniklooh':175000,
			'Casing Handphone':50000}

	var arrHargaBarang = Object.values(barang)
	
	var sortedHargaBarang = arrHargaBarang.sort(function(a, b){return b-a})

	if(memberId == null || memberId == ''){
		return 'Mohon maaf, toko X hanya berlaku untuk member saja'
	}
	else if(money < 50000){
		return 'Mohon maaf, uang tidak cukup'
	} else {
		barangDibeli = []
		change = money
		for(var i=0; i< arrHargaBarang.length; i++){
			if(change >= sortedHargaBarang[i]){

				barangDibeli.push(Object.keys(barang)[Object.values(barang).indexOf(sortedHargaBarang[i])]);
				change -= sortedHargaBarang[i];		
			}
		}
		object = {
			memberId: memberId,
			money: money,
			listPurchased: barangDibeli,
			changeMoney: change,
		};
		return object;
	}
}
console.log(shoppingTime('1820RzKrnWn08', 2475000))
console.log(shoppingTime('82Ku8Ma742', 170000))
console.log(shoppingTime('', 2475000))
console.log(shoppingTime('234JdhweRxa53', 15000))
console.log(shoppingTime())

//SOAL NOMOR 3
function naikAngkot(arrPenumpang) {
	var rute = ['A', 'B', 'C', 'D', 'E', 'F']
	var x = []

	
  if(Array.isArray(arrPenumpang)&&arrPenumpang.length){
		for(var i=0; i < arrPenumpang.length; i++){
        var b = rute.indexOf(arrPenumpang[i][1])
        var t = rute.indexOf(arrPenumpang[i][2])
        var bayar = t * 2000 - b * 2000
			  x[i] = {
				penumpang: arrPenumpang[i][0],
				naikDari: arrPenumpang[i][1],
				tujuan: arrPenumpang[i][2],
				bayar: bayar	
			}		
		}
	}

	return x

}


console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]))
console.log(naikAngkot([]))